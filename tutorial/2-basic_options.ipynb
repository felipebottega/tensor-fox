{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Basic options\n",
    "\n",
    "The *cpd* function has several options at disposal. Some of them may improve performance, precision or give insights about the tensor at hand. If you look at the source code, the first line of **cpd** is the following:\n",
    "\n",
    "    def cpd(T, r, options=False):\n",
    "\n",
    "The first action of the function **cpd** is to read the parameter $\\verb|options|$. When set to False, this function set the parameters to their default values. In order to change some of them the user needs to create the class $\\verb|options|$ and add the parameters of interest with their corresponding values. The default class with all the default parameters is showed below:\n",
    "\n",
    "    class options:\n",
    "        maxiter = 200  \n",
    "        tol = 1e-6\n",
    "        tol_step = 1e-6\n",
    "        tol_improv = 1e-6\n",
    "        tol_grad = 1e-6\n",
    "        method = 'dGN'\n",
    "        inner_method = 'cg'\n",
    "        cg_maxiter = 300\n",
    "        cg_factor = 1\n",
    "        cg_tol = 1e-6\n",
    "        bi_method_parameters = ['als', 500, 1e-6] \n",
    "        initialization = 'random'\n",
    "        trunc_dims = 0\n",
    "        tol_mlsvd = 1e-16\n",
    "        init_damp = 1\n",
    "        refine = False\n",
    "        symm = False\n",
    "        constraints = [0, 0, 0]\n",
    "        factors_norm = 0\n",
    "        trials = 10\n",
    "        display = 0\n",
    "        epochs = 1\n",
    "\n",
    "There are a lot of options, but don't worry, I will explain them one by one now. If you don't want to bother learning the details, be assured that all default values were obtained after a long and exhausting marathon of tests, with lots of different tensors. Of course we can't say these values will apply to any possible tensor, but you can learn more about these options as the necessity arises. The thing is, tensors are hard, very hard. Just there isn't a single algorithm which works for all of them, and very often you will need to make experimentation with the parameters in order to find the right combination for one specific tensor. In my experience, having several options to combine and tune works better than just having a few options like tolerance and number of iterations. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import TensorFox as tfx"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Display\n",
    "\n",
    "There are four choices for the $\\verb|display|$ option: $-2,-1,0,1,2,3,4$. These options controls what the user can see during the computations (works as the *verbose* parameter, but I prefer the name $\\verb|display|$). In the previous lesson we let the defaults and there were no output whatsoever (because the display default is $0$).\n",
    "\n",
    " - $\\verb|display|$ $=0$ (default): show nothing on the screen.\n",
    "\n",
    " - $\\verb|display|$ $=1$: shows useful information about the principal stages of the computation. \n",
    "    \n",
    " - $\\verb|display|$ $=2$: shows everything the option $\\verb|display|$ $=1$ shows plus information about each iteration.\n",
    "    \n",
    " - $\\verb|display|$ $=3$ is special, it shows eveything the option $\\verb|display|$ $=2$ shows and also shows the relative error of the compressed tensor (the computation of this error is costly so avoid that for big tensors).\n",
    "    \n",
    " - $\\verb|display|$ $=4$ is almost equal to $\\verb|display|$ $=3$ but now there are more digits displayed on the screen ($\\verb|display|$ $=3$ is a \"clean\" version of $\\verb|displa|y$ $=4$, with less information). \n",
    "    \n",
    " - $\\verb|display|$ $=-1$ is a special option for it is reserved for tensors of order higher than $3$. \n",
    " \n",
    " - $\\verb|display|$ $=-2$ shows everything the option $\\verb|display|$ $=-1$ shows plus the error of the compressed tensor (very costly) and the errors of the tensor train approximation before and after the CPD.\n",
    "    \n",
    "The options $-1$ and $-2$ will be discussed in the lesson of advanced options. Now let's start creating our toy model tensor."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create tensor.\n",
    "m = 2\n",
    "T = np.zeros((m, m, m))\n",
    "s = 0\n",
    "\n",
    "for k in range(m):\n",
    "    for i in range(m):\n",
    "        for j in range(m):\n",
    "            T[i,j,k] = s\n",
    "            s += 1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "-----------------------------------------------------------------------------------------------\n",
      "Computing MLSVD\n",
      "    No compression detected\n",
      "    Working with dimensions (2, 2, 2)\n",
      "-----------------------------------------------------------------------------------------------\n",
      "Type of initialization: random\n",
      "-----------------------------------------------------------------------------------------------\n",
      "Computing CPD\n",
      "===============================================================================================\n",
      "Final results\n",
      "    Number of steps = 48\n",
      "    Relative error = 1.3168379366319825e-05\n",
      "    Accuracy =  99.99868 %\n"
     ]
    }
   ],
   "source": [
    "# Create class of options with display=1.\n",
    "class options:\n",
    "    display = 1\n",
    "\n",
    "# Compute the CPD of T with partial display.\n",
    "R = 3\n",
    "factors, T_approx, output = tfx.cpd(T, R, options)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "-----------------------------------------------------------------------------------------------\n",
      "Computing MLSVD\n",
      "    No compression detected\n",
      "    Working with dimensions (2, 2, 2)\n",
      "-----------------------------------------------------------------------------------------------\n",
      "Type of initialization: random\n",
      "-----------------------------------------------------------------------------------------------\n",
      "Computing CPD\n",
      "    Iteration | Rel error |  Step size  | Improvement | norm(grad) | Predicted error | # Inner iterations\n",
      "        1     | 9.75e-01  |  2.06e+00   |  9.75e-01   |  3.33e+01  |    2.69e-01     |        2        \n",
      "        2     | 8.63e-01  |  2.14e+00   |  1.12e-01   |  7.92e+00  |    1.95e-01     |        3        \n",
      "        3     | 3.62e-01  |  3.08e+00   |  5.01e-01   |  1.19e+01  |    1.60e+00     |        3        \n",
      "        4     | 1.08e-01  |  8.11e-01   |  2.54e-01   |  1.19e+01  |    2.37e-01     |        3        \n",
      "        5     | 6.36e-02  |  2.27e-01   |  4.45e-02   |  3.13e+00  |    2.14e-03     |        5        \n",
      "        6     | 5.08e-02  |  1.29e-01   |  1.28e-02   |  1.27e+00  |    1.87e-04     |        5        \n",
      "        7     | 4.23e-02  |  1.39e-01   |  8.50e-03   |  6.00e-01  |    1.75e-03     |        4        \n",
      "        8     | 3.41e-02  |  1.92e-01   |  8.26e-03   |  3.25e-01  |    1.70e-03     |        5        \n",
      "        9     | 2.94e-02  |  1.65e-01   |  4.65e-03   |  2.70e-01  |    3.04e-02     |        7        \n",
      "       10     | 2.40e-02  |  1.54e-01   |  5.44e-03   |  4.05e-01  |    8.25e-04     |        6        \n",
      "       11     | 2.24e-02  |  6.96e-02   |  1.57e-03   |  1.36e-01  |    6.91e-03     |        4        \n",
      "       12     | 1.96e-02  |  2.43e-01   |  2.87e-03   |  2.13e-01  |    1.95e-03     |        6        \n",
      "       13     | 1.67e-02  |  7.85e-02   |  2.86e-03   |  2.08e-01  |    1.61e-03     |        4        \n",
      "       14     | 1.58e-02  |  1.61e-01   |  9.44e-04   |  7.04e-02  |    4.76e-03     |        7        \n",
      "       15     | 3.00e-02  |  5.23e-01   |  1.42e-02   |  1.54e-01  |    5.82e-03     |        9        \n",
      "       16     | 2.87e-02  |  2.81e-01   |  1.25e-03   |  4.88e-01  |    1.35e-04     |        10       \n",
      "       17     | 1.71e-02  |  1.68e-01   |  1.16e-02   |  5.07e-01  |    1.16e-03     |        6        \n",
      "       18     | 1.70e-02  |  1.28e-01   |  7.97e-05   |  4.41e-01  |    1.79e-04     |        9        \n",
      "       19     | 1.15e-02  |  1.38e-01   |  5.50e-03   |  3.28e-01  |    6.34e-04     |        8        \n",
      "       20     | 1.05e-02  |  9.36e-02   |  1.03e-03   |  3.11e-01  |    9.34e-06     |        15       \n",
      "       21     | 7.68e-03  |  6.41e-02   |  2.83e-03   |  2.30e-01  |    4.40e-07     |        13       \n",
      "       22     | 6.66e-03  |  5.04e-02   |  1.02e-03   |  1.90e-01  |    6.73e-07     |        9        \n",
      "       23     | 5.03e-03  |  4.12e-02   |  1.63e-03   |  1.53e-01  |    4.41e-07     |        7        \n",
      "       24     | 4.19e-03  |  3.20e-02   |  8.41e-04   |  1.20e-01  |    6.00e-07     |        8        \n",
      "       25     | 3.24e-03  |  2.61e-02   |  9.54e-04   |  9.49e-02  |    5.32e-07     |        7        \n",
      "       26     | 2.65e-03  |  2.05e-02   |  5.87e-04   |  7.62e-02  |    2.06e-07     |        8        \n",
      "       27     | 2.07e-03  |  1.66e-02   |  5.76e-04   |  5.99e-02  |    3.19e-07     |        7        \n",
      "       28     | 1.69e-03  |  1.31e-02   |  3.85e-04   |  4.85e-02  |    4.89e-07     |        7        \n",
      "       29     | 1.33e-03  |  1.05e-02   |  3.59e-04   |  3.88e-02  |    1.89e-07     |        7        \n",
      "       30     | 1.08e-03  |  8.36e-03   |  2.54e-04   |  3.12e-02  |    1.86e-07     |        7        \n",
      "       31     | 8.52e-04  |  6.72e-03   |  2.24e-04   |  2.48e-02  |    8.64e-08     |        7        \n",
      "       32     | 6.98e-04  |  5.29e-03   |  1.54e-04   |  1.99e-02  |    1.12e-06     |        6        \n",
      "       33     | 5.48e-04  |  4.17e-03   |  1.50e-04   |  1.73e-02  |    6.14e-07     |        6        \n",
      "       34     | 3.79e-04  |  2.95e-03   |  1.69e-04   |  1.38e-02  |    8.26e-07     |        4        \n",
      "       35     | 1.80e-04  |  1.76e-03   |  1.99e-04   |  1.02e-02  |    5.62e-07     |        3        \n",
      "       36     | 7.07e-05  |  7.83e-04   |  1.09e-04   |  4.63e-03  |    2.08e-07     |        2        \n",
      "       37     | 3.25e-05  |  2.28e-04   |  3.82e-05   |  1.92e-03  |    4.18e-07     |        1        \n",
      "       38     | 1.31e-05  |  8.42e-05   |  1.94e-05   |  1.31e-03  |    7.59e-09     |        1        \n",
      "       39     | 1.19e-05  |  2.26e-05   |  1.19e-06   |  1.78e-04  |    5.44e-09     |        1        \n",
      "       40     | 1.14e-05  |  1.81e-05   |  5.53e-07   |  1.32e-04  |    5.10e-09     |        1        \n",
      "===============================================================================================\n",
      "Final results\n",
      "    Number of steps = 40\n",
      "    Relative error = 1.13767118151859e-05\n",
      "    Accuracy =  99.99886 %\n"
     ]
    }
   ],
   "source": [
    "# Compute the CPD of T with full display.\n",
    "options.display = 2\n",
    "factors, T_approx, output = tfx.cpd(T, R, options)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "-----------------------------------------------------------------------------------------------\n",
      "Computing MLSVD\n",
      "    No compression detected\n",
      "    Working with dimensions (2, 2, 2)\n",
      "    Compression relative error = 1.994874e-16\n",
      "-----------------------------------------------------------------------------------------------\n",
      "Type of initialization: random\n",
      "    Initial guess relative error = 9.365165e-01\n",
      "-----------------------------------------------------------------------------------------------\n",
      "Computing CPD\n",
      "    Iteration | Rel error |  Step size  | Improvement | norm(grad) | Predicted error | # Inner iterations\n",
      "        1     | 5.78e-01  |  1.89e+00   |  5.78e-01   |  1.16e+01  |    5.68e-01     |        2        \n",
      "        2     | 2.86e-01  |  8.21e-01   |  2.92e-01   |  1.28e+01  |    2.36e-03     |        3        \n",
      "        3     | 2.02e-01  |  2.54e-01   |  8.42e-02   |  6.36e+00  |    2.89e-04     |        3        \n",
      "        4     | 1.39e-01  |  2.75e-01   |  6.29e-02   |  4.61e+00  |    4.23e-03     |        3        \n",
      "        5     | 1.01e-01  |  3.04e-01   |  3.84e-02   |  2.78e+00  |    3.38e-02     |        3        \n",
      "        6     | 7.07e-02  |  3.73e-01   |  3.03e-02   |  1.43e+00  |    3.90e-03     |        5        \n",
      "        7     | 6.38e-02  |  1.86e-01   |  6.86e-03   |  4.98e-01  |    4.92e-04     |        5        \n",
      "        8     | 6.27e-02  |  1.01e-01   |  1.11e-03   |  1.28e-01  |    8.91e-05     |        6        \n",
      "        9     | 6.11e-02  |  1.50e-01   |  1.59e-03   |  6.39e-02  |    5.28e-03     |        5        \n",
      "       10     | 4.92e-02  |  6.14e-01   |  1.19e-02   |  1.25e-01  |    6.99e-03     |        7        \n",
      "       11     | 2.74e-02  |  7.69e-01   |  2.18e-02   |  2.16e-01  |    1.14e-02     |        6        \n",
      "       12     | 2.13e-02  |  1.65e-01   |  6.18e-03   |  5.15e-01  |    1.29e-02     |        5        \n",
      "       13     | 1.43e-02  |  2.55e-01   |  6.98e-03   |  2.60e-01  |    3.06e-03     |        5        \n",
      "       14     | 9.12e-03  |  1.88e-01   |  5.15e-03   |  1.32e-01  |    5.06e-06     |        11       \n",
      "       15     | 5.54e-03  |  1.46e-01   |  3.58e-03   |  1.37e-01  |    1.70e-04     |        12       \n",
      "       16     | 3.30e-03  |  3.41e-02   |  2.23e-03   |  1.22e-01  |    3.07e-04     |        7        \n",
      "       17     | 2.44e-03  |  2.59e-02   |  8.67e-04   |  5.24e-02  |    5.04e-04     |        5        \n",
      "       18     | 1.52e-03  |  4.78e-02   |  9.16e-04   |  2.43e-02  |    2.93e-05     |        9        \n",
      "       19     | 7.41e-04  |  7.52e-03   |  7.80e-04   |  3.83e-02  |    5.47e-06     |        6        \n",
      "       20     | 4.88e-04  |  1.33e-02   |  2.54e-04   |  8.52e-03  |    5.70e-07     |        6        \n",
      "       21     | 2.85e-04  |  2.21e-03   |  2.03e-04   |  1.15e-02  |    6.52e-07     |        4        \n",
      "       22     | 2.75e-04  |  2.81e-04   |  9.56e-06   |  1.75e-03  |    9.92e-07     |        1        \n",
      "       23     | 2.72e-04  |  1.38e-04   |  3.09e-06   |  1.34e-03  |    9.41e-07     |        1        \n",
      "       24     | 2.69e-04  |  1.33e-04   |  2.91e-06   |  9.70e-04  |    8.14e-07     |        1        \n",
      "       25     | 2.66e-04  |  1.31e-04   |  2.71e-06   |  1.15e-03  |    8.69e-07     |        1        \n",
      "       26     | 2.64e-04  |  1.22e-04   |  2.72e-06   |  9.46e-04  |    7.25e-07     |        1        \n",
      "       27     | 2.61e-04  |  1.24e-04   |  2.57e-06   |  1.22e-03  |    8.34e-07     |        1        \n",
      "       28     | 2.58e-04  |  1.15e-04   |  2.63e-06   |  9.39e-04  |    6.66e-07     |        1        \n",
      "       29     | 2.56e-04  |  1.21e-04   |  2.49e-06   |  1.26e-03  |    8.12e-07     |        1        \n",
      "       30     | 2.53e-04  |  1.09e-04   |  2.59e-06   |  9.39e-04  |    6.21e-07     |        1        \n",
      "       31     | 2.51e-04  |  1.18e-04   |  2.43e-06   |  1.27e-03  |    7.95e-07     |        1        \n",
      "       32     | 2.48e-04  |  1.06e-04   |  2.55e-06   |  9.40e-04  |    5.83e-07     |        1        \n",
      "       33     | 2.46e-04  |  1.17e-04   |  2.38e-06   |  1.26e-03  |    7.79e-07     |        1        \n",
      "       34     | 2.43e-04  |  1.02e-04   |  2.52e-06   |  9.40e-04  |    5.50e-07     |        1        \n",
      "       35     | 2.41e-04  |  1.15e-04   |  2.34e-06   |  1.25e-03  |    7.63e-07     |        1        \n",
      "       36     | 2.39e-04  |  9.93e-05   |  2.50e-06   |  9.40e-04  |    5.20e-07     |        1        \n",
      "       37     | 2.36e-04  |  1.14e-04   |  2.30e-06   |  1.22e-03  |    7.47e-07     |        1        \n",
      "       38     | 2.34e-04  |  9.67e-05   |  2.47e-06   |  9.39e-04  |    4.92e-07     |        1        \n",
      "       39     | 2.32e-04  |  1.13e-04   |  2.26e-06   |  1.20e-03  |    7.31e-07     |        1        \n",
      "       40     | 2.29e-04  |  9.43e-05   |  2.44e-06   |  9.36e-04  |    4.67e-07     |        1        \n",
      "       41     | 2.27e-04  |  1.13e-04   |  2.23e-06   |  1.17e-03  |    7.15e-07     |        1        \n",
      "       42     | 2.25e-04  |  9.21e-05   |  2.41e-06   |  9.33e-04  |    4.43e-07     |        1        \n",
      "       43     | 2.22e-04  |  1.12e-04   |  2.19e-06   |  1.15e-03  |    6.98e-07     |        1        \n",
      "===============================================================================================\n",
      "Final results\n",
      "    Number of steps = 43\n",
      "    Relative error = 0.00022233902058443143\n",
      "    Accuracy =  99.97777 %\n"
     ]
    }
   ],
   "source": [
    "# Compute the CPD of T with full display plus relative error of compression.\n",
    "options.display = 3\n",
    "factors, T_approx, output = tfx.cpd(T, R, options)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The difference between display $2$ and $3$ is only the initial guess relative error, which is given when $\\verb|display|$ $=3$. This is the error $\\|T - T_{approx}^{(0)}\\| / \\|T\\|$, where $T_{approx}^{(0)}$ is the starting point of the iterations. Sometimes it can be useful to know if the starting point is too far away or not from the objective tensor. Since the computation of this error is very costly, I've made this as an extra option. \n",
    "\n",
    "Let $(X^{(k)}, Y^{(k)}, Z^{(k)})$ be the approximated factor matrices at the $k$-th iteration and define the error function \n",
    "$$F(w^{(k)}) = \\frac{1}{2} \\| T - T^{(k)}_{approx} \\|^2,$$ \n",
    "where $w^{(k)} = vec( vec(X^{(k)}), vec(Y^{(k)}), vec(Z^{(k)}) )$ is the vectorization of $(X^{(k)}, Y^{(k)}, Z^{(k)})$ and $T_{approx}^{(k)} = T^{(k)}_{approx}(X, Y, Z)$ is its corresponding coordinate tensor. Below there are the description of each output column.\n",
    "\n",
    " - **Iteration**: it just the numbering of the iterations.\n",
    " \n",
    " - **Rel error**: the relative error between the current approximation and the objective tensor, i.e., the value\n",
    " \n",
    " $$ \\frac{\\| T - T_{approx}^{(k)} \\|}{\\| T \\|},$$\n",
    " where $k$ is the numbering of the current iteration.\n",
    " \n",
    " - **Step size**: the distance between two consecutives CPDs, i.e., it is the value \n",
    " \n",
    " $$ \\| w^{(k-1)} - w^{(k)} \\|.$$\n",
    "\n",
    " \n",
    " - **Improvement**: the difference (in absolute value) between two consecutive errors, i.e., the value\n",
    " \n",
    " $$\\left| \\frac{\\| T - T_{approx}^{(k-1)} \\|}{\\| T \\|} - \\frac{\\| T - T_{approx}^{(k)} \\|}{\\| T \\|} \\right|.$$ \n",
    "\n",
    " - **norm(grad)**: the original problem can be regarded as a nonlinear least squares problem, and a minimizer is also a critical point, so it is of interest to keep track of the infinite norm of the gradient (the value $\\| \\nabla F(w^{(k)}) \\|_\\infty$) to check if it is approaching zero. \n",
    " \n",
    " - **Predicted error**: each iteration tries to minimize a linear model of the original problem. After we compute such a minimizer we have a error of this model which is expected to be close to the original one. In this case we are keeping track of the absolute error. This error is important for updating the damping parameter. For more about this linear model check the section about the damping parameter in the next lesson.\n",
    " \n",
    " - **# Inner iterations**: the linear model mentioned above is solved by the [conjugate gradient](https://en.wikipedia.org/wiki/Conjugate_gradient_method) method. There are parameters to control the error tolerance and number of iterations of this method. We talk more about this at the *advanced options* lesson."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Maximum number of iterations and tolerance\n",
    "\n",
    "As the names suggest, $\\verb|maxiter|$ is the maximum number of iterations permitted, whereas $\\verb|tol|$ is the tolerance parameter, which is used to make stopping criteria. Both values are related in the sense we should increase *maxiter* when we decrease $\\verb|tol|$. One can note that this is not the only tolerance parameter. All parameters $\\verb|tol|, \\ \\verb|tol|\\_\\verb|step|, \\ \\verb|tol|\\_\\verb|improv|, \\ \\verb|tol|\\_\\verb|grad|$ are used at each iteration as stopping conditions. The program stops if \n",
    "\n",
    "$1) \\hspace{1cm} \\displaystyle \\frac{\\| T - T_{approx}^{(k)} \\|}{\\| T \\|} <  \\verb|tol| \\hspace{6.6cm} (\\text{relative error})$\n",
    "\n",
    "$2) \\hspace{1cm} \\displaystyle \\| w^{k} - w^{k-1}\\| < \\verb|tol|\\_\\verb|step| \\hspace{5.6cm} (\\text{step size})$\n",
    "\n",
    "$3) \\hspace{1cm} \\displaystyle \\left| \\frac{\\| T - T_{approx}^{(k)} \\|}{\\| T \\|} - \\frac{\\| T - T_{approx}^{(k-1)} \\|}{\\| T \\|} \\right| <  \\verb|tol|\\_\\verb|improv| \\hspace{1.5cm} (\\text{relative improvement})$\n",
    "\n",
    "$4) \\hspace{1cm} \\displaystyle \\| \\nabla F(w^{(k)}) \\|_\\infty < \\verb|tol|\\_\\verb|grad| \\hspace{5.6cm}(\\text{gradient norm})$ \n",
    "\n",
    "Now let's decrease all tolerances to $10^{-12}$ and see if we get better approximations for the CPD of this example. We keep the rest with default values."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "scrolled": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "-----------------------------------------------------------------------------------------------\n",
      "Computing MLSVD\n",
      "    No compression detected\n",
      "    Working with dimensions (2, 2, 2)\n",
      "-----------------------------------------------------------------------------------------------\n",
      "Type of initialization: random\n",
      "-----------------------------------------------------------------------------------------------\n",
      "Computing CPD\n",
      "===============================================================================================\n",
      "Final results\n",
      "    Number of steps = 200\n",
      "    Relative error = 1.9943022066457984e-07\n",
      "    Accuracy =  99.99998 %\n"
     ]
    }
   ],
   "source": [
    "# Compute the CPD of T with tol = 1e-12.\n",
    "options.display = 1\n",
    "options.tol = 1e-12\n",
    "options.tol_step = 1e-12\n",
    "options.tol_improv = 1e-12\n",
    "options.tol_grad = 1e-12\n",
    "factors, T_approx, output = tfx.cpd(T, R, options)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To understad better how the tolerance influence the precision we can make a plot varying the tolerances. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAnEAAAEWCAYAAAAJlMFHAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAADl0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uIDMuMC4zLCBodHRwOi8vbWF0cGxvdGxpYi5vcmcvnQurowAAIABJREFUeJzt3XucZHV57/vPFxBxGBA3yHiB6QZFEgSNYbzlxgxKRBN1mxADdtjbiM5Bw87F494BMUp2wiExHt1JZKsTZY+XkRGJ8YxsthqFQVRIAC8IIhFxBkbU8QY6thrR5/yx1mBN05fqmaruqurP+/Wq19TvV2ut3/PUqu5+5rculapCkiRJw2WvxQ5AkiRJ82cRJ0mSNIQs4iRJkoaQRZwkSdIQsoiTJEkaQhZxkiRJQ8giThoBSU5OcttixzFqkqxP8t+6WO5LSZ7ah/GvSPK7vd7uKEqyX5JKcthixyItFIs4aUAk2dHx+GmSH3S0JxY7vkGSZGOSH7XvzbeTfDDJUb0ep6peWFWv7WK5R1XVNX0Y/8Sqes/urJvka0l+pZfxJFmZ5LJ225XkYVNef1CSdyT5bpK7kpzVy/El7coiThoQVbV85wO4A3h2R9+Gfo2bZJ9+bbvP/qJ9r1YC3wP+YbqFhji/QfQT4DLg+TO8/v8Aj6TZJycDr0myemFCk5YeizhpSLSzHBcm+WqSbUn+JskDZlj28CT/X5JvJrk9yZkdr/1VkncneU+S7wGnJvnlJP+S5J52BuUNO4ufjsNUL2kPG34nyRumjPeyJF9I8r0kn0ty3FxxTJPb55O8pG3vk+T6bg5lVtUOYCNw7Cz57Z3kz9oYvplkQ5KDOsZfneTaNv87kryg7d+Y5FXt84e1M353J/lWkis61r9v1mu2/bTzsHeSVyb5RpKvzDbL2sb0e+3zM5N8NMnftTF8KcnT53p/ZtjuH7TrfyvJ+5Ks6HjtN5J8sR3jf3TGUFVfqao3A5+aYdP/Cfjzqrq7qm4E1gMvnCGGn0vy8fY9/0aSd3S89vg0h5K/0763/3fbP+PndJrtP6iN/852G3+f5IG78XZJA8siThoefw48DjgOOB5YDdyvyEmyN3A58EngETQzIq9MckLHYr8NvB14MPCPwI+Bs4D/APwq8GzgxVM2/UzgCcAvAr+/c4YlyenAnwKnAQcCpwDf6TIOAKrqB8DvAX+d5NHAa4BJ4HVzvSlJDmzH/vQs+f1X4NeBXwEOa/N9Q7v+o2lml/4GOJjmvb15mqH+FLgVOAR4OHDeDCHNtZ/GgNC8J2cBb06yfK48W78GXN/G+UbgrV2ud58kzwL+DHgezazZN4F3ta89DHgP8CfAQ4G72hy62e7DaT4/n+3o/izw2BlWuQB4P3AQzczdW9rtPAT4CPA+4GHAY4CPtet08znd6Q00+/o44Oh2O2d3k4s0NKrKhw8fA/YAtgBPn9L3FeDEjvZzgS+0z08GbmufnwB8ccq6fw68qX3+V8CH5xj/bODi9vl+QAGrOl7fBPxx+/wq4P+aZhuzxjHDuOcCtwDfAsZnWW4j8APgbuCrwD8BYzPlB3wZ+OWO9hE0RWLamC6eZZxXtc9fC7wXOHKa5b4G/EqX++keYK+O178L/MIM418L/F77/Ezgpo7X/kO7Xw6aYd37YprSvwH47x3tg4Cf0hRMa4ErO17bC9i+M4aO/uXt2A/r6Duq7UtH37N35j5NHJfQFKIPn9L/+8A1Xf6cTPc5PQzYB/h34JEdy64BbtmTn0sfPgbt4UycNASShOaP7NaO7q00MylTjQHj7eGwu5PcDby8XX+nO6ds/5gk/yfJ15N8F3g1zYxTp691PJ+k+UMOcDjwpd2MY6qLgEcD76+qLbMsB3B+VR1UVQ+vqudVVed7c19+7Xt3OHB5RxyfpilQDp4l/vuNRzMzdWV7SPTlUxfocj99o6p+2tHufC/nMnUfMI91d3pEZ3xVdTdNIfnI9rU7O177KU1R2o0d7b8HdPQdSHO+4nT+BFgGfDrJjTsP2TLL/ujyc0qbxwOAmzv2+fuBQ7vMRRoKFnHSEKiqovkDPtbRvZLp/8DeSTP7cVDH44Cqel7nJqes8w805zk9qqoOBP47zSxVN+4EHrWbcUz1FprDaM9L8sQux5/Offm1793O2bHOWParqm/OEv+uG6y6p6r+qKrGaA7XvirJL09ZZj77abHcRUd8SR5MU2x9hWZW87CO1/Zi+v8o3E9VfRX4NvD4ju7HM/2haao5v+5FNIem/xC4KMlKZt8f3X5Ovwrc2y63c38/uKoO7iYXaVhYxEnD42Kaq/0OTnIozaHHd02z3McBkvxxmosS9knyuCS/OMu2DwDuqaodSR4LvGQecb0VOLs9GT1JHpPmXl3ziiPNRQ2PoTkR/hXAO5M8aB5xzObNwF8lObwd69Akz25fewfwm0mel+YCiIcmedw08T0nyRHtbNs9NFdq/mSasbrdTwth3/a93/nYu43vJUmOTbIf8NfAFVX1NZrD5E9O8qz2goGXAw/p3GC7zs4LBB445WKBdwKvTvLgNBe3vJDm4ob7SfK7SR7RFr53t9330syYPTrJS5Psm+TAjoK+q89pVf2YZlb3b5Mc0n4uD09yUpfvmzQULOKk4fFq4PM0MxufAT5Bc57WLto/YM8CfonmsNk3gDcx+2G3PwFenGQHcCHNye1dqap3Aq8HLqU5LHcpzXlaXceR5FFtLqdX1Q+q6iLgC9Plt5teS3Oy/BVprlj9JM0FGlTVl2jOW3sl8B2aCwemOxn/54HNNIcHPwa8rqqunWa5rvbTAvkozbmDOx/nVNVlNBcVbKKZlXsYcDrcN5t2GvB3NBc8HAZ8DvgR3FfA/aB9DZpzN+/pGO+VNDOR24AP05x7t3mG2J4K3NB+5t4LrK2qu6rqO8BJwKk05+PdSnNBCszvc/rHbX7XtzF+kOZQvTQy0vwnSJKkXbWzcV+juWdhz29mLGnPOBMnSbpPkme2h0P342e3erlhkcOSNA2LOElSp1+juSXLduBpwPOq6t8XNyRJ0/FwqiRJ0hByJk6SJGkILYkvhj7kkENqfHy87+N8//vfZ//99+/7OP02KnmAuQyqUcllVPIAcxlEo5IHmMt83XDDDd+sqofOtdySKOLGx8e5/vrr+z7O5s2bWb16dd/H6bdRyQPMZVCNSi6jkgeYyyAalTzAXOYryda5l/JwqiRJ0lCyiJMkSRpCFnGSJElDyCJOkiRpCA1lEZfk55O8OcmlSV662PFIkiQttAUv4pJclGR7kpum9J+c5NYktyU5e7ZtVNUtVXUm8HxgVT/jXVI2bIDxcU448UQYH2/akiRpIC3GTNx64OTOjiR7AxcCzwSOAU5LckyS45JcNuVxaLvOc4CPAx9d2PBH1IYNsHYtbN1KqmDr1qZtISdJ0kBalK/dSjIOXFZVx7btpwLnVdUz2vY5AFV1QRfb+t9V9RvT9K8F1gKsWLHi+I0bN/Ys/pns2LGD5cuX932cfnjKqaey39e/fr/+H65YwbUL8N71yzDvk6nMZfCMSh5gLoNoVPIAc5mvNWvW3FBVcx5pHJSb/T4SuLOjvQ148kwLJ1kN/BbwQODy6ZapqnXAOoBVq1bVQtxkcKhvZrh9+7Td+23fPrw5MeT7ZApzGTyjkgeYyyAalTzAXPplUIq4TNM34xRhVW0GNvcrmCVp5crmEOp0/ZIkaeAMytWp24DDO9qHAXctUixL0/nnw7Jlu/YtW9b0S5KkgTMoRdx1wFFJjkiyL3AqsGmRY1paJiZg3ToYG6MSGBtr2hMTix2ZJEmaxmLcYuRi4Brg6CTbkpxRVfcCZwEfAm4BLqmqmxc6tiVvYgK2bOGqK66ALVss4CRJGmALfk5cVZ02Q//lzHCRgiRJknY1KIdTJUmSNA8WcZIkSUPIIk6SJGkIWcRJkiQNIYs4SZKkIWQRJ0mSNIQs4iRJkoaQRZwkzdeGDTA+zgknngjj401bkhbYgt/sV5KG2oYNsHYtTE4SgK1bmzb4LSeSFpQzcZI0H+eeC5OTu/ZNTjb9krSALOIkaT7uuGN+/ZLUJxZxveD5MdLSsXLl/PolqU8s4vbUzvNjtm4lVT87P8ZCThpN558Py5bt2rdsWdMvSQvIIm5PeX6MtLRMTMC6dTA2RiUwNta0vahB0gKziNtTnh8jLT0TE7BlC1ddcQVs2WIBJ2lRWMTtKc+PkSRJi8Aibk95fowkSVoEFnF7yvNjJEnSIrCI6wXPj5EkSQvMIk6SJGkIWcRJkiQNIYs4SZKkITSURVyS1UmuTvLmJKsXOx5JkqSFtuBFXJKLkmxPctOU/pOT3JrktiRnz7GZAnYA+wHb+hWrJEnSoNpnEcZcD7wReMfOjiR7AxcCJ9EUZdcl2QTsDVwwZf0XAVdX1VVJVgCvB7wcVJIkLSmpqoUfNBkHLquqY9v2U4HzquoZbfscgKqaWsBN3c6+wLur6pRpXlsLrAVYsWLF8Rs3buxlCtPasWMHy5cv7/s4/TYqeYC5DKpRyWVU8gBzGUSjkgeYy3ytWbPmhqpaNddyizETN51HAnd2tLcBT55p4SS/BTwDOIhmVu9+qmodsA5g1apVtXr16l7FOqPNmzezEOP026jkAeYyqEYll1HJA8xlEI1KHmAu/TIoRVym6ZtxirCq3ge8r3/hSJIkDbZBuTp1G3B4R/sw4K5FikWSJGngDUoRdx1wVJIj2vPcTgU2LXJMkiRJA2sxbjFyMXANcHSSbUnOqKp7gbOADwG3AJdU1c0LHZskSdKwWPBz4qrqtBn6LwcuX+BwJEmShtKgHE6VJEnSPFjESZIkDSGLOEmSpCFkESdJkjSELOIkSZKGkEWcJEnSELKIkyRJGkIWcZIkSUPIIk6SJGkIWcRJkiQNIYs4SZKkIWQRJ0mSNIQs4iRJkoaQRZwkSdIQsoiTJEkaQhZxkiQNkg0bYHycE048EcbHm7Y0jX0WOwBJktTasAHWroXJSQKwdWvTBpiYWMzINICciZMkaVCcey5MTu7aNznZ9EtTWMRJkjQo7rhjfv1a0iziJEkaFCtXzq9fS5pFnCRJg+L882HZsl37li1r+qUphrKIS/KrSd6c5K1JPrnY8UiS1BMTE7BuHYyNUQmMjTVtL2rQNBa8iEtyUZLtSW6a0n9ykluT3Jbk7Nm2UVVXV9WZwGXA2/sZr4aUl+hLGlYTE7BlC1ddcQVs2WIBpxktxi1G1gNvBN6xsyPJ3sCFwEnANuC6JJuAvYELpqz/oqra3j5/AfDifgesIeMl+pKkJSBVtfCDJuPAZVV1bNt+KnBeVT2jbZ8DUFVTC7jObawE/qyqXjLD62uBtQArVqw4fuPGjb1MYVo7duxg+fLlfR+n34Y9j6eceir7ff3r9+v/4YoVXLsAn4N+Gfb90mlUchmVPMBcBtGo5AHmMl9r1qy5oapWzbXcoNzs95HAnR3tbcCT51jnDOB/zfRiVa0D1gGsWrWqVq9evYchzm3z5s0sxDj9NvR5bN8+bfd+27cPdV5Dv186jEouo5IHmMsgGpU8wFz6ZVAubMg0fbNOEVbVa6rKixp0f16iL0laAgaliNsGHN7RPgy4a5Fi0bDzEn1J0hIwaxGXZK8kz1+AOK4DjkpyRJJ9gVOBTQswrkaRl+hLkpaAWYu4qvopcFYvB0xyMXANcHSSbUnOqKp723E+BNwCXFJVN/dyXC0xXqIvSRpx3VzY8M9JXgG8B/j+zs6q+vbuDFhVp83Qfzlw+e5sU5Ikaanppoh7UfvvH3T0FXBk78ORJElSN+Ys4qrqiIUIRJIkSd2bs4hL8gDgpcCvtV2bgbdU1Y/7GJckSZJm0c3h1DcBDwD+Z9s+ve3z664kSZIWSTdF3BOr6vEd7SuSfLZfAUmSJGlu3dzs9ydJHrWzkeRI4Cf9C0mSJElz6WYm7r8CVya5nebrscaA3+9rVJIkSZrVrEVckr2AHwBHAUfTFHFfqKofLUBskiRJmsGsRVxV/TTJ/1tVTwVuXKCYJEmSNIduzon7cJLfTpK+RyNJkqSudHNO3MuB/YF7k/yQ5pBqVdWBfY1MkiRJM5rrnLgAj62qOxYoHkmSJHVh1sOpVVXAPy1QLJIkSepSN+fEXZvkiX2PRJIkSV3r5py4NcCZSbYA3+dn58Q9rp+BSZIkaWbdFHHP7HsUkiRJmpc5D6dW1VbgcODE9vlkN+tJkiSpf+YsxpK8BvhT4Jy26wHAu/oZlCRJkmbXzYza84Dn0JwPR1XdBRzQz6AkSZI0u26KuH9vbzVSAEn2729IkiRJmks3RdwlSd4CHJTkJcBHgH/ob1iSJEmazZxXp1bV65KcBHwXOBp4dVX9c98jm0WSY4DzgG8BH62qSxczHkmSpIXWzS1GaIu2nhRuSS4CfhPYXlXHdvSfDPwtsDfw1qr6q1k280zg76vq6iSbAIs4SZK0pCzGrULWAyd3diTZG7iQpjg7BjgtyTFJjkty2ZTHocA7gVOT/A1w8ALHL2l3bdgA4+OccOKJMD7etCVJu6WrmbheqqqPJRmf0v0k4Laquh0gyUbguVV1Ac2s3XT+oC3+3tevWCX10IYNsHYtTE4SgK1bmzbAxMRiRiZJQynNhadzLJQ8CFhZVbf2ZNCmiLts5+HUJKcAJ1fVi9v26cCTq+qsWdZ/JbA/8Kaq+vg0y6wF1gKsWLHi+I0bN/Yi9Fnt2LGD5cuX932cfhuVPMBcBslTTj2V/b7+9fv1/3DFCq5dgJ/Pfhj2fdLJXAbPqOQB5jJfa9asuaGqVs25YFXN+gCeDdwKfLlt/wKwaa715tjmOHBTR/t3aM6D29k+neact90eo/Nx/PHH10K48sorF2ScfhuVPKrMZaAkVXD/R7LYke22od8nHcxl8IxKHlXmMl/A9dVFfdPNOXHn0RzuvLst+j7TFmG9tI3mq712Ogy4q8djSFpMK1fOr1+SNKtuirh7q+qePsdxHXBUkiOS7AucCmzq85iSFtL558OyZbv2LVvW9EuS5q2bIu6mJC8A9k5yVJK/Bz65uwMmuRi4Bjg6ybYkZ1TVvcBZwIeAW4BLqurm3R1DGimjckXnxASsWwdjY1QCY2NN24saJGm3dHN16n8BzgV+BLybptD6y90dsKpOm6H/cuDy3d2uNJJG7YrOiQmYmOCqzZtZvXr1YkcjSUOtm5m4o6vq3Kp6Yvt4VVX9sO+RSYJzz4XJyV37JiebfknSktZNEff6JF9I8hdJHtv3iCT9zB13zK9fkrRkzFnEVdUaYDXwDWBdks8leVW/A5OEV3RKkmbU1dduVdXXqurvgDOBzwCv7mtUkhpe0SlJmsGcRVySn09yXpKbgDfSXJl6WN8jk+QVnZKkGXVzder/Ai4Gfr2qvAGvtNC8olOSNI05i7iqespCBCJJkqTuzVjEJbmkqp6f5HNAdb4EVFU9ru/RSZIkaVqzzcT9Ufvvby5EIJIkSerejBc2VNVX26cvq6qtnQ/gZQsTniRJkqbTzS1GTpqm75m9DkSSJEndm+2cuJfSzLgdmeTGjpcOAD7R78AkSZI0s9nOiXs38H+AC4CzO/q/V1Xf7mtUkiRJmtWMRVxV3QPcA5wGkORQYD9geZLlVeWXN0qSJC2Sbr6x4dlJvgh8GbgK2EIzQydJkqRF0s2FDX8JPAX4t6o6AnganhMnSZK0qLop4n5cVd8C9kqyV1VdCfxCn+OSJC2EDRtgfJwTTjwRxsebtqSh0M13p96dZDnwMWBDku3Avf0NS5LUdxs2wNq1MDlJALZubdrQfGevpIHWzUzcc4EfAH8CfBD4EvDsfgYlSVoA554Lk5O79k1ONv2SBt6cM3FV9f2O5tv7GIskaSHdMcNNBmbqlzRQZrvZ7/eY5ovvd/5bVQf2OTZJUj+tXNkcQp2uX9LAm+27Uw+oqgM7Hgd0/ruQQUqS+uD882HZsl37li1r+iUNvG7OiSPJryT5/fb5IUmO6G9Yu4x9ZJK3Jbl0tj5J0jxNTMC6dTA2RiUwNta0vahBGgrd3Oz3NcCfAue0XfsC7+pm40kuSrI9yU1T+k9OcmuS25KcPdP6AFV1e1WdMVefJGk3TEzAli1cdcUVsGWLBZw0RLqZiXse8Bzg+wBVdRdwQJfbXw+c3NmRZG/gQuCZwDHAaUmOSXJcksumPA7tchxJ0lLnPe+0xKSqZl8g+deqelKST1XVLybZH7imqh7X1QDJOHBZVR3btp8KnFdVz2jb5wBU1QVzbOfSqjplrr6O19YCawFWrFhx/MaNG7sJd4/s2LGD5cuX932cfhuVPMBcBtWo5DIqecDw53LoRz7C0a97HXv/6Ef39f3kgQ/k1le8gu1Pf/oiRrb7hn2fdDKX+VmzZs0NVbVqzgWratYH8ArgLcDtwEuAa4A/nGu9jvXHgZs62qcAb+1onw68cZb1DwbeTHN/unNm6pvtcfzxx9dCuPLKKxdknH4blTyqzGVQjUouo5JH1QjkMjZWBfd/jI0tdmS7bej3SQdzmR/g+uqixurmPnGvS3IS8F3gaODVVfXP3dWS08p0w8wy/reAM+fqkyQtYd7zTktQN1+7RVu0/TM057Qlmaiq3T3ZYBtweEf7MOCu3dyWJEne805L0owXNiQ5MMk5Sd6Y5NfTOIvmsOrz92DM64CjkhyRZF/gVGDTHmxPkrTUec87LUGzXZ36TprDp58DXgx8GPgd4LlV9dxuNp7kYppz6I5Osi3JGVV1L3AW8CHgFuCSqrp5D3KQJC113vNOS9Bsh1OPrKrjAJK8FfgmsLKqvtftxqvqtBn6Lwcun0+gkiTNamICJia4avNmVq9evdjRSH0320zcj3c+qaqfAF+eTwEnSZKk/pltJu7xSb7bPg/woLYdoMrvT5UkSVo0MxZxVbX3QgYiSZKk7nXztVuSJEkaMBZxkiRJQ8giTpIkaQhZxEmSJA0hizhJkqQhZBEnSZI0hCziJEmShpBFnCRJ0hCyiJMkSRpCFnGSJElDyCJOkiRpCFnESZIkDSGLOEmSpCFkESdJkjSELOIkSZKGkEWcJEnSELKIkyRJGkIWcZIkSUNo4Iu4JEcmeVuSSzv6fj7Jm5NcmuSlixmfJEnSYuhrEZfkoiTbk9w0pf/kJLcmuS3J2bNto6pur6ozpvTdUlVnAs8HVvU+ckmSpMHW75m49cDJnR1J9gYuBJ4JHAOcluSYJMcluWzK49CZNpzkOcDHgY/2L3xJkqTBtE8/N15VH0syPqX7ScBtVXU7QJKNwHOr6gLgN+ex7U3ApiT/G3h3byKWJEkaDqmq/g7QFHGXVdWxbfsU4OSqenHbPh14clWdNcP6BwPnAycBb62qC5KsBn4LeCBwY1VdOM16a4G1ACtWrDh+48aNPc7s/nbs2MHy5cv7Pk6/jUoeYC6DalRyGZU8wFwG0ajkAeYyX2vWrLmhquY8XayvM3EzyDR9M1aSVfUt4MwpfZuBzbMNUlXrgHUAq1atqtWrV88zzPnbvHkzCzFOv41KHmAug2pUchmVPMBcBtGo5AHm0i+LcXXqNuDwjvZhwF2LEIckSdLQWowi7jrgqCRHJNkXOBXYtAhxSJIkDa1+32LkYuAa4Ogk25KcUVX3AmcBHwJuAS6pqpv7GYckSdKo6ffVqafN0H85cHk/x5YkSRplA/+NDZIkSbo/izhJkqQhZBEnSZI0hCziJEmShpBFnCRJ0hCyiJMkSRpCFnGSJElDyCJOkiRpCFnESZIkDSGLOEmSpCFkESdJkjSELOIkSZKGkEWcJEnqjw0bYHycE048EcbHm7Z6Zp/FDkCSJI2gDRtg7VqYnCQAW7c2bYCJicWMbGQ4EydJknrv3HNhcnLXvsnJpl89YREnSZJ674475tevebOIkyRJvbdy5fz6NW8WcZIkqffOPx+WLdu1b9mypl89YREnSZJ6b2IC1q2DsTEqgbGxpu1FDT1jESdJkvpjYgK2bOGqK66ALVss4HrMIk6SJGkIWcRJkiQNoYEv4pIcmeRtSS7t6Fud5Ookb06yehHDkyRJWhR9LeKSXJRke5KbpvSfnOTWJLclOXu2bVTV7VV1xtRuYAewH7Ctt1FLkiQNvn5/7dZ64I3AO3Z2JNkbuBA4iaYAuy7JJmBv4IIp67+oqrZPs92rq+qqJCuA1wOeKSlJkpaUVFV/B0jGgcuq6ti2/VTgvKp6Rts+B6CqphZwU7dzaVWdMqVvX+DdU/vb19YCawFWrFhx/MaNG/c8mTns2LGD5cuX932cfhuVPMBcBtWo5DIqeYC5DKJRyQPMZb7WrFlzQ1Wtmmu5fs/ETeeRwJ0d7W3Ak2daOMnBwPnAE5KcU1UXJPkt4BnAQTQzffdTVeuAdQCrVq2q1atX9yb6WWzevJmFGKffRiUPMJdBNSq5jEoeYC6DaFTyAHPpl8Uo4jJN34zTgVX1LeDMKX3vA97X47gkSZKGxmJcnboNOLyjfRhw1yLEIUmSNLQWo4i7DjgqyRHtOW2nApsWIQ5JkqSh1e9bjFwMXAMcnWRbkjOq6l7gLOBDwC3AJVV1cz/jkCRJGjV9PSeuqk6bof9y4PJ+ji1JkjTKBv4bGyRJkhbdhg0wPs4JJ54I4+NNe5EtxtWpkiRJw2PDBli7FiYnm1tsbN3atAEmFu/7BpyJkyRJms2558Lk5K59k5NN/yKyiJMkSZrNHXfMr3+BWMRJkiTNZuXK+fUvEIs4SZKk2Zx/PixbtmvfsmVN/yKyiJMkSZrNxASsWwdjY1QCY2NNexEvagCLOEmSpLlNTMCWLVx1xRWwZcuiF3BgESdJkjSULOIkSZKGkEWcJEnSELKIkyRJGkIWcZIkSUMoVbXYMfRdkm8AWxdgqEOAby7AOP02KnmAuQyqUcllVPIAcxlEo5IHmMt8jVXVQ+daaEkUcQslyfVVtWqx49hTo5IHmMugGpVcRiUPMJdBNCp5gLn0i4dTJUmShpBFnCRJ0hCyiOutdYsdQI+MSh5gLoNqVHIZlTzAXAbRqOQB5tIXnhMnSZI0hJyJkyRJGkIWcZIkSUPIIq4LSS5Ksj3JTbux7r5J1iX5tyRfSPLb/Yixy1h2K48kByT5TMfjm0n+R7/i7DKmPdmiQ5t9AAAIO0lEQVQnpyX5XJIbk3wwySH9iHEe8exJLr/b5nFzktf2I745xt+T2M9PcmeSHVP6H5jkPUluS/IvScZ7Fe8c8fQjl19L8qkk9yY5pXfRzhlPP3J5eZLPt5+3jyYZ613EM8bSjzzObH/+P5Pk40mO6V3Es8bT81w6Xj8lSSVZkNte9Gm/vDDJNzr+zry4dxHPGk9f9kuS57c/LzcneXdvor0/i7jurAdO3s11zwW2V9VjgGOAq3oV1G5Yz27kUVXfq6pf2PmguXHy+3od3DytZzdySbIP8LfAmqp6HHAjcFZvQ5u39exeLgcDfwM8raoeC6xI8rQexzaX9ez+z8YHgCdN038G8J2qejTwBuCvd3P787We3udyB/BCoG+/xGewnt7n8mlgVftzcymwEP9pWE/v83h3VR3X/i57LfD63dz+fK2n97mQ5ADgD4F/2c1t74719CEX4D0df2veupvbn6/19DiXJEcB5wC/3P5u/uPdjm4OFnFdqKqPAd/u7EvyqHYW54YkVyf5uRlWfxFwQbudn1bVot2xeg/z2Ln8UcChwNV9DHVOe5BL2sf+SQIcCNzV/4hntge5HAn8W1V9o21/BFjQmd49+UxV1bVV9dVpXnou8Pb2+aXA09p91Vf9yKWqtlTVjcBP+xP19PqUy5VVNdk2rwUO63ng9x+zH3l8t6O5P7AgV/f16WcF4C9oitEf9jbimfUxlwXXp1xeAlxYVd9pl9ve88A7gvDRxQMYB27qaH8UOKp9/mTgimnWOQi4k+Z/ep8C3gusGLY8pqz/auB1i70/9iQX4BTgu8BXgY8Bew9jLsBDgG3tuvsA/wh8YBhin7L+jintm4DDOtpfAg4Zxlw6+tcDpwzzfpny2huBVw1rHsAftJ+rO3duaxhzAZ4A/GP7fDPNTOmw5vLC9nfyjTT/eTt8iHN5P01h/Qma//Cc3K/Y90HzlmQ58EvAezsmCB44zaL70Pxv9RNV9fIkLwdeB5y+IIHOYR55dDqVAYm/U7e5JHkA8FKaX363A39PM+39lwsT6dy6zaWqvpPkpcB7aGZ6PkkzO7dodvMzdb/NTNO34PdC6lEuA6GXuST5PWAVcEJvopvX2D3Jo6ouBC5M8gLgVcB/7lmQXdrTXJLsRXO6wQt7Htw89Wi/fAC4uKp+lORMmtn4E3sXZXd6lMs+wFHAapoa4Ookx1bV3b2Ks3Mgzd9ewN3VnFNxnyR7Aze0zU3Aa4BJ4J/avvfSnO8zKLrKo6pe3fY/Htinqm5g8HS7Tz4AUFVfal+/BDh7AePsRtf7pao+QJtTkrXATxY00vub12dqBtuAw4Ft7TmMD2bK4Y4F0otcBkVPcknydJrzfE+oqh/1JdLZ9XqfbATe1MP45mNPczkAOBbY3BYbDwM2JXlOVV3fp5hnssf7paq+1dH8BxbuXNipevU77Nqq+jHw5SS30hR11/U6WIu43VBV303y5SS/U1Xvbc/XeVxVfRaYuuM/QFONXwE8Dfj8ggc8g/nk0ToNuHhho+xOt7kkeQRwTJKHVnMu2UnALYsU9rTm+fk6tKq2J3kI8DLg+YsR80678ZmaziaamZFraA59X1HtMYqF1KNcBkIvcknyBOAtNIeG+neOzyx6lMdRVfXFtvkbwBdnW75f9jSXqroHuO/K+iSbgVcsQgHXq/3y8PrZ+WXPYZF+L/fo5/79NH8v16e5+8FjaI789CVgH3MfL7+Y5lj9j2kq7DOAI4APAp+lKcxePcO6YzTnXd1Ic5x95TDm0a5/O/Bzi70/erBPzqT5BXEjzSzWwUOcy8Xt658HTh2y2F/brvPT9t/z2v79aGatbwP+FThyiHN5Ytv+PvAt4OYhzuUjwNeBz7SPTUOax98CN7c5XAk8dlj3yZRlNrNA58T1ab9c0O6Xz7b7ZUH+1vQpl9CcC/954HP08XezX7slSZI0hLzFiCRJ0hCyiJMkSRpCFnGSJElDyCJOkiRpCFnESZIkDSHvEydpZCU5mObWPtDcDPUnwM7vmn1SVf37lOX3Ab5ZVQctXJSStHu8xYikJSHJeTTfcfi6WZaZVxHX3gg0VbWgX3AvSeDhVElLVJL/luSm9vFfZljm7CT/muTGJDu/fu7R7TpvBj4FPDzJuiTXJ7l553LtstuSnJfk0+02HtP2H5Dk7Uk+1/b/x7b/mUmuSfKpJO9Jsn//3wlJw8oiTtKSk+RJwATwJOCpwMuSPG7KMs8CVgJPpvm6nV9K8kvty8cAb6uqJ1TVV4Czq2oV8HjgpCTHdGzq61X1BOCtwMvbvvOAb1TVce06VyU5lOZ7fJ9WVb9I840if9Tj1CWNEM+Jk7QU/Srwj1U1CZDk/cCvsOt3G/868Ezg0217Oc13IG4HvlRVnV9mfVqSM2h+pz6Cpsjbua33tf/eADyrff504D8CVHNOy3fa2bhjgE+2X2i+L/DxXiQraTRZxElaitLlMn9ZVW/bpTN5NM13oe5sH0UzY/akqro7ybtovv91px+1//6En/3ODTD1hOQAH6yq07vOQtKS5uFUSUvRx4DnJXlQkuXAc4GrpyzzIeCMneelJTksySHTbOtA4HvAd5M8HHhGF+N/GDir3W6SPAT4JHBCkiPb/v3bAlGSpuVMnKQlp6r+NcnFwM5Dom+qqs+1V6fuXObyJD8HXNse3vwe8IJpNvcpmkOnNwG3A5/oIoQ/B/5nkptoZuj+rKo2tYdk35Nk33a5VwJfnH+GkpYCbzEiSZI0hDycKkmSNIQs4iRJkoaQRZwkSdIQsoiTJEkaQhZxkiRJQ8giTpIkaQhZxEmSJA2h/x94RhxV/Ow0twAAAABJRU5ErkJggg==\n",
      "text/plain": [
       "<Figure size 720x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "errors = []\n",
    "options.display = 0\n",
    "tolerances = [1e-6, 1e-7, 1e-8, 1e-9, 1e-10, 1e-11, 1e-12, 1e-13, 1e-14, 1e-15, 1e-16]\n",
    "tolerances_str = ['1e-6','1e-7','1e-8','1e-9','1e-10','1e-11','1e-12','1e-13','1e-14','1e-15','1e-16']\n",
    "\n",
    "for tol in tolerances:\n",
    "    options.maxiter = 500\n",
    "    options.tol = tol\n",
    "    options.tol_step = tol\n",
    "    options.tol_improv = tol\n",
    "    options.tol_grad = tol\n",
    "    factors, T_approx, output = tfx.cpd(T, R, options)\n",
    "    errors.append(output.rel_error)\n",
    "    \n",
    "plt.figure(figsize=[10,4])\n",
    "plt.plot(tolerances_str, errors, 'ro')\n",
    "plt.title('Tolerance x Precision in Log10 scale')\n",
    "plt.xlabel('Tolerance')\n",
    "plt.ylabel('Relative error')\n",
    "plt.yscale('log')\n",
    "plt.grid()\n",
    "plt.show()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
