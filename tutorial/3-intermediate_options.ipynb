{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Intermediate options\n",
    "\n",
    "In the previous lesson we talked about the following options:\n",
    "\n",
    "    display\n",
    "    maxiter  \n",
    "    tol \n",
    "    tol_step\n",
    "    tol_improv\n",
    "    tol_grad\n",
    "    \n",
    "These options tend to be the standard in many iterative algorithms and the user may be satisfied with this. In fact, Tensor Fox was constructed in order to be as robust as possible. The several extra options should be used only when the options above are not enough (and when this happens, be sure you are handling a difficult tensor).\n",
    "\n",
    "The following options will be introduced now:\n",
    "\n",
    "    tol_mlsvd\n",
    "    trunc_dims\n",
    "    initialization\n",
    "    refine    \n",
    "    init_damp\n",
    "    symm    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import TensorFox as tfx"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create and print the tensor.\n",
    "m = 2\n",
    "T = np.zeros((m, m, m))\n",
    "s = 0\n",
    "\n",
    "for k in range(m):\n",
    "    for i in range(m):\n",
    "        for j in range(m):\n",
    "            T[i,j,k] = s\n",
    "            s += 1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Truncation\n",
    "\n",
    "In several applications of linear algebra, often one have to compute the truncated SVD of some matrix. By truncating we can reduce the dimensionality of the problem, which leads to lots of speed up. On the other hand, we lost information after the truncation. Ideally, we want to truncate as much as possible while maintaining the relevant information.\n",
    "\n",
    "The same can be made for tensors, but in this context we use the [multilinear singular value decomposition](https://epubs.siam.org/doi/abs/10.1137/s0895479896305696) (MLSVD). If $T$ is a $L$-order tensor, then its MLSVD is written as $T = (U_1, \\ldots, U_L) \\cdot S$, where each $U_l$ is a orthogonal matrix and $S$ is a tensor with the same shape as $T$ (we consider $S$ as the *compressed* version of $T$). The notation used stands for the [multilinear multiplication](https://en.wikipedia.org/wiki/Multilinear_multiplication) brtween the $L$-tuple and the tensor $S$. This tensor $S$ is called the *central tensor* and it is the analogous of $\\Sigma$ in the classical SVD of the form $A = U \\Sigma V^T$. Note that we've said that $S$ is of the same shape as $T$, and just as in the 2D linear algebra, this is the *full* MLSVD, in contrast to the *reduced* MLSVD. In the same way we can consider the reduced SVD $A = U \\Sigma V^T$ where $\\Sigma$ is $R \\times R$ (and $R$ is the rank of $A$), we can have $S$ of shape $R_1 \\times R_2 \\times \\ldots \\times R_L$. The tuple $(R_1, R_2, \\ldots, R_L)$ is the *multilinear rank* of $T$. \n",
    "\n",
    "The level of trnucation is controlled by the parameter $\\verb|tol| \\_ \\verb|mlsvd|$, which is $10^{-6}$ by default. The program computes the SVD of each unfolding $T_{(\\ell)}$. Then it computes the errors \n",
    "$$\\| T_{(\\ell)} - \\tilde{T}_{(\\ell)} \\|^2 / \\|T_{(\\ell)}\\|^2,$$ where $\\tilde{T}_{(\\ell)}$ is obtained by a truncated SVD of $T_{(\\ell)}$. The program increases the rank of the truncations sequentially, until the condition \n",
    "$$\\| T_{(\\ell)} - \\tilde{T}_{(\\ell)} \\|^2 / \\|T_{(\\ell)}\\|^2 < \\verb|tol| \\_ \\verb|mlsvd|$$ \n",
    "is satisfied. Two special values are the following.\n",
    "\n",
    "**1)** $\\verb|tol| \\_ \\verb|mlsvd| = 0$: compress the tensor (that is, compute its MLSVD) but do not truncate the central tensor of the MLSVD.\n",
    "\n",
    "**2)** $\\verb|tol| \\_ \\verb|mlsvd| = -1$: use the original tensor, so the computation of the MLSVD is not performed.\n",
    " \n",
    "If you are working with tensor with order higher than $3$, then you can pass $\\verb|tol| \\_ \\verb|mlsvd|$ as a list with two values. The first one works for the truncation of the original high order tensor. The second one works for the truncations of the associated tensor train third order CPD's (see next lesson). If you set $\\verb|tol| \\_ \\verb|mlsvd|$ to a single value, the program assumes you want to use this value for both high order and third order tensors. Finally, we want to mention that you can also use the parameter $\\verb|trunc| \\_ \\verb|dims|$ to tell the program the exactly truncation you want to use. Just set this parameter to be a list of the dimensions you want (default is $\\verb|trunc| \\_ \\verb|dims| = 0$, which lets the program to decide the best truncation). If you are work with tensors with order higher than $3$, this parameter refers to the original high order tensor only. \n",
    "\n",
    "In the example we are working it is possible to note that the program is unable to truncate. Still, we can force some truncation, say $2 \\times 1 \\times 1$, and see if we get good precision of this. The precision can already be antecipated just by seeing the relative error of the compression (remember that this requires setting $\\verb|display|$ to $3$, which can be costly), that is, the line\n",
    "\n",
    "    Compression relative error = 0.130828\n",
    "\n",
    "That line says this is the best precision we can get. This is because this is the error of the truncated $S$ and $T$, and all the iterations to be made will try to obtain a CPD for $S$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "scrolled": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "-----------------------------------------------------------------------------------------------\n",
      "Computing MLSVD\n",
      "    Compression detected\n",
      "    Compressing from (2, 2, 2) to (2, 1, 1)\n",
      "-----------------------------------------------------------------------------------------------\n",
      "Type of initialization: random\n",
      "-----------------------------------------------------------------------------------------------\n",
      "Computing CPD\n",
      "===============================================================================================\n",
      "Final results\n",
      "    Number of steps = 25\n",
      "    Relative error = 0.13082808698628967\n",
      "    Accuracy =  86.91719 %\n"
     ]
    }
   ],
   "source": [
    "class options:\n",
    "    display = 1\n",
    "    trunc_dims = [2,1,1]\n",
    "\n",
    "R = 3\n",
    "factors, T_approx, output = tfx.cpd(T, R, options)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Initialization\n",
    "\n",
    "The iteration process needs a starting point for iterating. This starting point depends on the $\\verb|initialization|$ option, and there are three possible choices in this case: $\\verb|smart|, \\ \\verb|smart| \\_ \\verb|random|, \\ \\verb|random|$ and $\\verb|user|$. Both $\\verb|smart|$ and $\\verb|smart| \\_ \\verb|random|$ options generates a CPD of rank $R$ with a strategy relying on the MLSVD. The strategy $\\verb|smart|$ maximizes the energy of the initialization wheareas $\\verb|smart| \\_ \\verb|random|$ makes almost the same, but with a chance to take some different entries. These strategies generates starting points with small relative error, so it is already close to the objective tensor. Although this seems to be a good thing, there is also a risk to be close to a local minimum or saddle point, and in this cases these methods will always fail. The $\\verb|random|$ is more robust, this option generates a CPD of rank $R$ with entries drawn from the normal distribution. The relative error in this case usually is close to $1$. Finally, there is the $\\verb|user|$ option where the user provides a list $[X, Y, Z]$ as starting point. This is a good idea when we already have a close CPD and want to increase its precision."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "-----------------------------------------------------------------------------------------------\n",
      "Computing MLSVD\n",
      "    No compression detected\n",
      "    Working with dimensions (2, 2, 2)\n",
      "-----------------------------------------------------------------------------------------------\n",
      "Type of initialization: smart_random\n",
      "-----------------------------------------------------------------------------------------------\n",
      "Computing CPD\n",
      "===============================================================================================\n",
      "Final results\n",
      "    Number of steps = 25\n",
      "    Relative error = 1.0189887686424392e-06\n",
      "    Accuracy =  99.9999 %\n"
     ]
    }
   ],
   "source": [
    "# Compute the CPD of T with random initialization. \n",
    "# Notice we need to set trunc_dims to zero so the program can decide which truncation to use.\n",
    "options.trunc_dims = 0\n",
    "options.initialization = 'smart_random'\n",
    "factors, T_approx, output = tfx.cpd(T, R, options)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "-----------------------------------------------------------------------------------------------\n",
      "Computing MLSVD\n",
      "    No compression detected\n",
      "    Working with dimensions (2, 2, 2)\n",
      "-----------------------------------------------------------------------------------------------\n",
      "Type of initialization: user\n",
      "-----------------------------------------------------------------------------------------------\n",
      "Computing CPD\n",
      "===============================================================================================\n",
      "Final results\n",
      "    Number of steps = 7\n",
      "    Relative error = 0.1307980758576221\n",
      "    Accuracy =  86.92019 %\n"
     ]
    }
   ],
   "source": [
    "# Compute the CPD of T with user initialization.\n",
    "X = np.ones((m, R))\n",
    "Y = np.ones((m, R))\n",
    "Z = np.ones((m, R))\n",
    "options.initialization = [X,Y,Z]\n",
    "factors, T_approx, info = tfx.cpd(T, R, options)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Refinement\n",
    "\n",
    "As we mentioned before, the user may give an initial CPD as starting point for our iterative algorithm, which may be a good idea when it is desired to increase the precision of the CPD. This process can be done automatically by setting $\\verb|refine|$ to True. This option makes the program runs the algorithm two times, where the second run uses the approximated CPD computed in the first run as starting point. However, this second run is made in the original space (the space of the tensor $T$). Ideally, we want to compress and limit ourselves to the compressed version of $T$, but if this is not enough, the $\\verb|refine|$ option can squeeze more precision at a cost of working with uncompressed tensors. This options obly work for third order tensors. If you are working with a high order tensor, the program will use this options only for the intermediate third order tensors."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "scrolled": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "-----------------------------------------------------------------------------------------------\n",
      "Computing MLSVD\n",
      "    No compression detected\n",
      "    Working with dimensions (2, 2, 2)\n",
      "    Compression relative error = 1.994874e-16\n",
      "-----------------------------------------------------------------------------------------------\n",
      "Type of initialization: random\n",
      "    Initial guess relative error = 1.048501e+00\n",
      "-----------------------------------------------------------------------------------------------\n",
      "Computing CPD\n",
      "    Iteration | Rel error |  Step size  | Improvement | norm(grad) | Predicted error | # Inner iterations\n",
      "        1     | 8.26e-01  |  3.00e+00   |  8.26e-01   |  1.22e+01  |    1.90e-01     |        2        \n",
      "        2     | 6.65e-01  |  2.91e+00   |  1.61e-01   |  1.14e+01  |    4.48e-02     |        3        \n",
      "        3     | 1.78e-01  |  9.22e-01   |  4.87e-01   |  4.93e+01  |    3.11e-01     |        3        \n",
      "        4     | 9.77e-02  |  3.02e-01   |  8.00e-02   |  7.71e+00  |    2.74e-03     |        4        \n",
      "        5     | 8.19e-02  |  1.79e-01   |  1.58e-02   |  2.16e+00  |    4.16e-03     |        3        \n",
      "        6     | 6.32e-02  |  2.59e-01   |  1.88e-02   |  1.26e+00  |    2.59e-03     |        4        \n",
      "        7     | 3.64e-02  |  3.15e-01   |  2.68e-02   |  6.82e-01  |    3.79e-04     |        6        \n",
      "        8     | 2.00e-02  |  1.56e-01   |  1.64e-02   |  3.70e-01  |    1.59e-04     |        5        \n",
      "        9     | 1.29e-02  |  6.28e-02   |  7.08e-03   |  2.24e-01  |    7.61e-06     |        6        \n",
      "       10     | 7.25e-03  |  5.78e-02   |  5.66e-03   |  1.62e-01  |    3.27e-05     |        5        \n",
      "       11     | 4.52e-03  |  2.87e-02   |  2.72e-03   |  1.63e-01  |    1.38e-05     |        5        \n",
      "       12     | 2.88e-03  |  1.74e-02   |  1.65e-03   |  1.28e-01  |    6.30e-07     |        7        \n",
      "       13     | 1.63e-03  |  1.72e-02   |  1.25e-03   |  8.68e-02  |    5.56e-07     |        11       \n",
      "       14     | 8.13e-04  |  7.26e-03   |  8.13e-04   |  6.64e-02  |    1.11e-07     |        10       \n",
      "       15     | 4.24e-04  |  2.77e-03   |  3.90e-04   |  3.52e-02  |    8.02e-09     |        10       \n",
      "       16     | 2.56e-04  |  1.26e-03   |  1.67e-04   |  1.69e-02  |    6.32e-10     |        11       \n",
      "       17     | 1.66e-04  |  8.71e-04   |  9.06e-05   |  8.86e-03  |    1.59e-10     |        12       \n",
      "       18     | 9.62e-05  |  9.25e-04   |  6.96e-05   |  5.34e-03  |    2.41e-09     |        11       \n",
      "       19     | 5.06e-05  |  4.33e-04   |  4.56e-05   |  3.82e-03  |    2.12e-10     |        12       \n",
      "       20     | 2.76e-05  |  1.69e-04   |  2.30e-05   |  2.13e-03  |    2.15e-11     |        11       \n",
      "       21     | 1.75e-05  |  7.89e-05   |  1.01e-05   |  1.06e-03  |    1.62e-12     |        11       \n",
      "       22     | 1.03e-05  |  9.21e-05   |  7.24e-06   |  5.76e-04  |    5.55e-12     |        13       \n",
      "       23     | 5.55e-06  |  4.50e-05   |  4.74e-06   |  4.02e-04  |    4.04e-13     |        14       \n",
      "       24     | 3.21e-06  |  1.79e-05   |  2.34e-06   |  2.25e-04  |    1.98e-12     |        8        \n",
      "       25     | 1.99e-06  |  1.15e-05   |  1.22e-06   |  1.15e-04  |    1.03e-12     |        8        \n",
      "       26     | 1.37e-06  |  6.89e-06   |  6.18e-07   |  6.61e-05  |    1.35e-11     |        5        \n",
      "\n",
      "===============================================================================================\n",
      "Computing refinement of solution\n",
      "    Initial guess relative error = 1.373326e-06\n",
      "-----------------------------------------------------------------------------------------------\n",
      "Computing CPD\n",
      "    Iteration | Rel error |  Step size  | Improvement | norm(grad) | Predicted error | # Inner iterations\n",
      "        1     | 1.25e-06  |  5.57e-07   |  1.25e-06   |  3.43e-05  |    5.37e-13     |        1        \n",
      "        2     | 1.19e-06  |  3.02e-07   |  5.92e-08   |  2.70e-05  |    1.01e-13     |        1        \n",
      "        3     | 1.12e-06  |  4.66e-07   |  7.62e-08   |  2.27e-05  |    3.80e-13     |        1        \n",
      "===============================================================================================\n",
      "Final results\n",
      "    Number of steps = 29\n",
      "    Relative error = 1.1161849433621431e-06\n",
      "    Accuracy =  99.99989 %\n"
     ]
    }
   ],
   "source": [
    "# Compute the CPD of T with refinement.\n",
    "options.display = 3\n",
    "options.initialization = 'random'\n",
    "options.refine = True\n",
    "factors, T_approx, output = tfx.cpd(T, R, options)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Damping parameter\n",
    "\n",
    "In the previous section we mentioned that, at each iteration, the program solves a minimization problem. This minimization is of the form\n",
    "\n",
    "$$\\min_x \\| Jx - b\\|,$$\n",
    "where $J$ is a tall matrix (the Jacobian matrix of the function of the residuals) and $x, b$ are vectors. There are two problems here: $J$ has too many rows and is not of full rank. In fact, the number of rows of the matrix brings the curse of dimensionality to the problem. One way to avoid this is to consider solving the normal equations\n",
    "\n",
    "$$J^T J x = J^Tb.$$\n",
    "\n",
    "Now the matrix has a reasonable size. To solve the problem of lack of full rank we introduce regularization, thus obtaining the new set of equations\n",
    "\n",
    "$$(J^T J + \\mu D) x = J^Tb$$\n",
    "where $\\mu > 0$ is the damping parameter and $D$ is a diagonal matrix. At each iteration the damping parameter is updated following a certain rule, and the user doesn't have influence over this. On the other hand, the user can choose the initial damping parameter factor. More precisely, the first damping parameter is $\\mu = \\tau \\cdot E[T]$, where $\\tau$ is the damping parameter factor and $E[T]$ is the mean of the values of $T$ (if there is compression, use $S$ instead of $T$). The default value we use is $\\tau = 1$, but the user can change it with the parameter $\\verb|init| \\_ \\verb|damp|$. Experience shows that this value has little influence on the overall process, but sometimes it has a noticeable influence, so be aware of that. Finally, we remark that it is possible to pass the parameter $\\verb|init| \\_ \\verb|damp|$ as a list of values, such that $\\verb|init| \\_ \\verb|damp|[k]$ will be the damping parameter used at $k$-th iteration.   "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Symmetric tensors\n",
    "\n",
    "If one want to work with symmetric tensors, just set $\\verb|symm|$ to True. With this option activated the initialization and all iterations of the dGN function will be done with symmetric tensors. At each iteration the approximated CPD is given by a triplet $X, Y, Z$. The next step is to make the assignements\n",
    "\n",
    "$$X \\leftarrow \\frac{X+Y+Z}{3}, \\quad Y \\leftarrow X, \\quad Z \\leftarrow X.$$\n",
    "\n",
    "If the objective tensor is really symmetric, then this procedure converges. Otherwise it can diverge or become unstable."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
